import constants from './constants.js';
import Basket from "./Basket.js";

const pricingRules = [
    {
        productId: 'FR1',
        rules: [
            {
                name: constants.promos.BOGOF
            }
        ]
    },
    {
        productId: 'SR1',
        rules: [
            {
                name: constants.promos.BULK,
                threshold: 3,
                discountPercentage: 10
            }
            
        ]
    }
]
const basket = new Basket(pricingRules);
basket.add('FR1');
basket.add('FR1');
console.log(basket.total());