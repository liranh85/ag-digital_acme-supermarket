const constants = {
    promos: {
        BOGOF: 'bogof',
        BULK: 'bulk'
    }
};

export default constants;