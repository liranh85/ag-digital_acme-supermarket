import constants from './constants.js';

class Basket {
    constructor(pricingRules) {
        this.pricingRules = pricingRules;
        this.products = [
            {
                id: 'FR1',
                name: 'Fruit tea',
                price: 3.11
            },
            
            {
                id: 'SR1',
                name: 'Strawberries',
                price: 5
            },
            {
                id: 'CF1',
                name: 'Coffee',
                price: 11.23
            }
        ];

        this.itemsInBasket = [];
    }

    add(item) {
        const productIndex = this.getObjectIndexInArrayById(item, this.products);
        // If we don't sell this product, return false
        if(productIndex === -1) {
            return false;
        }
        const indexInBasket = this.getObjectIndexInArrayById(item, this.itemsInBasket);
        // If product is not yet in the basket
        if(indexInBasket === -1) {
            const quantity = 1;
            this.itemsInBasket.push({
                id: item,
                quantity
            });
            return quantity;
        }
        // Otherwise (product in basket), increment quantity, and return it
        return ++this.itemsInBasket[indexInBasket].quantity;
    }

    total() {
        let total = 0;
        this.itemsInBasket.forEach((item, index, array) => {
            const productIndex = this.getObjectIndexInArrayById(item.id, this.products);
            const product = this.products[productIndex];
            let quantity = item.quantity;
            let price = product.price;
            const productIndexInRules = this.getObjectIndexInArrayById(item.id, this.pricingRules, 'productId');
            // If this product was find in the pricingRules (and thus has one or more rules assigned to it)
            if(productIndexInRules >= 0) {
                // debugger;
                const productRules = this.pricingRules[productIndexInRules].rules;
                const bogofIndexInRules = this.getObjectIndexInArrayById(constants.promos.BOGOF, productRules, 'name');
                
                // If this product has BOGOF
                if(bogofIndexInRules >= 0) {
                    quantity = Math.ceil(quantity / 2);
                }
                // debugger;
                const bulkIndexInRules = this.getObjectIndexInArrayById(constants.promos.BULK, productRules, 'name');

                // If this product has a discount on a bulk purchase
                if(bulkIndexInRules >= 0) {
                    const bulkRule = productRules[bulkIndexInRules];
                    // If quantity is at or above the threshold for a bulk purchase, apply discount
                    if(item.quantity >= bulkRule.threshold) {
                        price = price - price * (bulkRule.discountPercentage / 100);
                    }
                }
            }
            
            total += price * quantity;
        });

        return total;
    }

    getObjectIndexInArrayById(productId, haystack, idProperty = 'id') {
        for(let i = 0; i < haystack.length; i++) {
            if(haystack[i][idProperty] === productId) {
                return i;
            }
        }

        return -1;
    }
}

export default Basket;